from datetime import datetime

import requests
import queries
from Fronius.fronius_requests import FroniusApi


# Return current date rounded to 5 minutes
def get_current_date():
    minutes = int(datetime.now().minute / 5) * 5
    date = datetime.now().replace(minute=minutes).replace(second=0).strftime("%Y-%m-%dT%H:%M:%S").format()
    return date


# Return current day
def get_current_day():
    current_day = datetime.now().strftime("%Y-%m-%d").format()
    return current_day


class plant_def:
    frapi = FroniusApi()

    def __init__(self):
        self.session = requests.Session()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        return False

    # <-----------------PLANT BASE DATA------------------>

    # Add plant basic info
    def add_plant_base_info(self):
        plants = self.frapi.get_plants()
        for plant in plants['pvSystems']:
            id_planta = plant['pvSystemId']
            nome_cliente = plant['name']
            localizacao = plant['address']['city']
            capacidade = plant['peakPower'] / 1000
            foto = plant['pictureURL']
            queries.add_plant_basic_info(id_planta, nome_cliente, localizacao, capacidade, foto)

    def get_inverter_by_plant(self, plant_id):
        devices = self.frapi.get_devices(plant_id)
        for inverter in devices['devices']:
            id_inversor = inverter['deviceId']
            modelo_inversor = inverter['deviceName']
            capacidade = inverter['nominalAcPower'] / 1000
            status = inverter['isActive']
            queries.add_inverter(id_inversor, plant_id, modelo_inversor, capacidade, status)

    def add_inverters(self):
        plants = self.frapi.get_plant_ids()
        for plant in plants['pvSystemIds']:
            self.get_inverter_by_plant(plant)

    # <--------------------INVERTER BASE DATA-------------------->

    # Get the  phases params
    def get_phases_params(self, plant_id, device_id):
        date = get_current_date()
        invChannels = ['Voltage', 'Current']
        phasesParams = []
        logs = self.frapi.get_inverter_params(plant_id, device_id, date, date)
        i = 0
        for log in logs['data']:
            phasesParams.append(device_id)
            phasesParams.append(log['logDateTime'])
            for channels in log['channels']:
                i += 1
                if any(x in channels['channelName'] for x in invChannels) and not any(
                        x in channels['channelName'] for x in ['DC']):
                    print(channels['channelName'])
                    if i % 2 == 0:
                        phasesParams.append(channels['value'])
                        if phasesParams:
                            queries.add_phases_params(phasesParams)
                    else:
                        phasesParams.clear()
                        phasesParams.append(device_id)
                        phasesParams.append(log['logDateTime'])
                        phasesParams.append(channels['value'])

    # Get the inverter params
    def get_inverter_params(self, paramsInv, plant_id, device_id):
        date = get_current_date()
        queryParams = []
        logs = self.frapi.get_inverter_params(plant_id, device_id, date, date)
        i = 0
        for log in logs['data']:
            queryParams.append(device_id)
            queryParams.append(log['logDateTime'])
            for channels in log['channels']:
                if any(x in channels['channelName'] for x in paramsInv):
                    i += 1
                    if any('DC' in x for x in paramsInv):
                        if i % 2 == 0:
                            queryParams.append(channels['value'])
                            queryParams.append(channels['channelName'][-1])
                            queries.add_mppt(queryParams)
                        else:
                            queryParams.clear()
                            queryParams.append(device_id)
                            queryParams.append(log['logDateTime'])
                            queryParams.append(channels['value'])
                    else:
                        queryParams.append(channels['value'] / 1000)
                        queries.add_inverter_instant_generation(queryParams)

    # Get plant network status
    def get_plant_status(self):
        plants = self.frapi.get_plant_ids()
        for plant in plants['pvSystemIds']:
            status = self.frapi.get_plant_status(plant)
            monitoramento = int(status['status']['isOnline'])
            queries.update_plant_status(monitoramento, plant)

    # Get mppts info
    def get_mppt(self, plant_id, device_id):
        params = ['VoltageDC', 'CurrentDC']
        self.get_inverter_params(params, plant_id, device_id)

    # <---------------------INVERTER GENERATION DATA--------------------->

    # Get daily generation by inverter
    def get_daily_inverter_generation(self, plant_id, device_id, date_start, date_end):
        invDailyGeneration = self.frapi.get_daily_inverter_generation(plant_id, device_id, date_start, date_end)
        for generation in invDailyGeneration['data']:
            for channels in generation['channels']:
                if any(x in channels['channelName'] for x in ['EnergyExported']):
                    generation = round(channels['value'] / 1000, 2)
                    return generation

    # Get inverter generation per minute
    def get_inverter_instant_generation(self, plant_id, device_id):
        generationChannel = ['EnergyExported']
        self.get_inverter_params(generationChannel, plant_id, device_id)

    # <---------------------PLANT GENERATION DATA--------------------->

    # Get daily plant generation
    def get_daily_plant_generation(self, plant_id, date_start, date_end):
        dailyGeneration = self.frapi.get_daily_plant_generation(plant_id, date_start, date_end)
        for generation in dailyGeneration['data']:
            for channels in generation['channels']:
                if any(x in channels['channelName'] for x in ['EnergyOutput']):
                    generation = round(channels['value'] / 1000, 2)
                    return generation

    # Get instant plant generation
    def get_instant_plant_generation(self, plant_id):
        plantDailyGeneration = self.frapi.get_plant_status(plant_id)
        for generation in plantDailyGeneration['data']['channels']:
            if 'PowerOutput' in generation['channelName']:
                return round(generation['value'] / 1000, 2)


plf = plant_def()
# plf.get_inverter_instant_generation("f880dc81-53ca-4347-84f6-2b23a6307e33", "18d8eadb-9ab0-482b-aa66-500ffb354262")
plf.get_plant_status()