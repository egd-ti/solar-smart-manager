import mysql.connector

import configparser

config = configparser.ConfigParser()
config.read('config.ini')

try:
    connection = mysql.connector.connect(
        host=config['mysql']['host'],
        user=config['mysql']['user'],
        password=config['mysql']['password'],
        database=config['mysql']['db']
    )
    cursor = connection.cursor()
except mysql.connector.Error as err:
    raise 'Database connection failed for  ' + config['mysql']['user'] + '@ ' + config['mysql']['host'] + '/ ' \
          + config['mysql']['db']
    exit()


# Insert plant basic info
def add_plant_basic_info(id_planta, nome_cliente, localizacao, capacidade, foto):
    sql = "INSERT INTO planta_solar (id_planta_solar, nome_planta, localizacao, capacidade, foto) VALUES (%s, %s, %s, %s, %s)"
    val = (id_planta, nome_cliente, localizacao, capacidade, foto)
    cursor.execute(sql, val)
    connection.commit()


# Insert inverters
def add_inverter(id_inversor, id_planta, modelo, capacidade, status):
    sql = "INSERT INTO inversor (id_inversor, id_planta_solar, modelo, capacidade, status) VALUES (%s, %s, %s, %s, %s)"
    val = (id_inversor, id_planta, modelo, capacidade, status)
    cursor.execute(sql, val)
    connection.commit()


def update_plant_status(status, id_planta):
    sql = "UPDATE planta_solar SET monitoramento = %s WHERE id_planta_solar = %s"
    cursor.execute(sql, (status, id_planta))
    connection.commit()


# Insert inverter mppts
def add_mppt(mpptParams):
    sql = "INSERT INTO mppt (id_inversor, data, tensao, corrente, nome) VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(sql, tuple(mpptParams))
    connection.commit()


# Insert inverter params
def add_phases_params(phasesParams):
    sql = "INSERT INTO fase (id_inversor, data, tensao, corrente) VALUES (%s, %s, %s, %s)"
    cursor.execute(sql, tuple(phasesParams))
    connection.commit()


def add_inverter_instant_generation(generation):
    sql = "INSERT INTO inversor_params (id_inversor, data_horario, geracao) VALUES (%s, %s, %s)"
    cursor.execute(sql, tuple(generation))
    connection.commit()

def add_inverter_daily_generation(generation):
    sql = "INSERT INTO inversor_params (id_)"

def get_plants():
 sql = "SELECT * FROM "