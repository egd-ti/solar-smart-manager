import datetime
import requests


class FroniusApiError(RuntimeError):
    pass


# Return response body
def response_success(response) -> object:
    if response.status_code != 200:
        raise "Request failed: %s" % response
    data = response.json()
    return data


class FroniusApi:
    base_url = "https://api.solarweb.com/swqapi"
    headers = {"Accept": "application/json", "AccessKeyId": "FKIA534627BD82204EE09A42834D5867C476",
               "AccessKeyValue": "c48f4c78-8ca6-4e02-9318-b0061491cab3"}

    current_time = datetime.datetime.now()

    def __init__(self):
        self.session = requests.Session()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        return False

    # Return url
    def get_url(self, page):
        return self.base_url + page

    # Return all plants list (ids only)
    def get_plant_ids(self):
        response = self.session.get(self.get_url("/pvsystems-list"), headers=self.headers)
        return response_success(response)

    # Return all plants list and basic info
    def get_plants(self):
        response = self.session.get(self.get_url("/pvsystems"), headers=self.headers)
        return response_success(response)

    # Return plant basic info
    def get_plant_info(self, plant_id):
        response = self.session.get(self.get_url("/pvsystems/" + plant_id), headers=self.headers)
        return response_success(response)

    # Return status and instant plant generation
    def get_plant_status(self, plant_id):
        response = self.session.get(self.get_url("/pvsystems/" + plant_id + "/flowdata?timezone=local"), headers=self.headers)
        return response_success(response)

    # Return day generation and receipt
    def get_plant_data_day(self, plant_id):
        response = self.session.get(self.get_url(
            "/pvsystems/" + plant_id + "/aggdata/years/" + self.current_time.year + "/months/" + self.current_time.month + "/days/" + self.current_time.day),
            headers=self.headers)
        return response_success(response)

    # Return devices list of a plant
    def get_devices(self, plant_id):
        response = self.session.get(self.get_url("/pvsystems/" + plant_id + "/devices?type=inverter&isActive=true"),
                                    headers=self.headers)
        return response_success(response)

    # Return inverter info
    def get_inverter_info(self, plant_id, inv_id):
        response = self.session.get(self.get_url("/pvsystems/" + plant_id + "/devices/" + inv_id), headers=self.headers)
        return response_success(response)

    # Return inverter params per hour
    def get_inverter_params(self, plant_id, inv_id, date_start, date_end):
        response = self.session.get(self.get_url(
            "/pvsystems/" + plant_id + "/devices/" + inv_id + "/histdata?from=" + date_start + "&to=" + date_end + "-03:00&timezone=local"),
            headers=self.headers)
        return response_success(response)

    # Get daily plant generation response
    def get_daily_plant_generation(self, plant_id, date_start, date_end):
        response = self.session.get(
            self.get_url("/pvsystems/" + plant_id + "/aggrdata?from=" + date_start + "&to=" + date_end),
            headers=self.headers)
        return response_success(response)

    # Get daily plant generation response
    def get_daily_inverter_generation(self, plant_id, device_id, date_start, date_end):
        response = self.session.get(
            self.get_url("/pvsystems/" + plant_id + "/devices/" + device_id + "/aggrdata?channel=EnergyExported&from="
                         + date_start + "&to=" + date_end), headers=self.headers)
        return response_success(response)

    # Return devices count response
    def get_devices_count(self, plant_id):
        response = self.session.get(self.get_url("/pvsystems/" + plant_id + "/devices-count"), headers=self.headers)
        return response_success(response)
