# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Alerta(models.Model):
    id_alerta = models.IntegerField(primary_key=True)
    id_planta_solar = models.ForeignKey('PlantaSolar', models.DO_NOTHING, db_column='id_planta_solar')
    data_horario = models.DateTimeField(blank=True, null=True)
    tipo = models.CharField(max_length=45, blank=True, null=True)
    mensagem = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'alerta'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group_id = models.IntegerField()
    permission_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group_id', 'permission_id'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type_id = models.IntegerField()
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type_id', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.IntegerField()
    group_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user_id', 'group_id'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.IntegerField()
    permission_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user_id', 'permission_id'),)


class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    nome_cliente = models.CharField(max_length=45, blank=True, null=True)
    endereco = models.CharField(max_length=45, blank=True, null=True)
    contato = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cliente'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Fase(models.Model):
    id_fase = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey('Inversor', models.DO_NOTHING, db_column='id_inversor', related_name='fases')

    class Meta:
        managed = False
        db_table = 'fase'


class FaseParams(models.Model):
    id_fase_params = models.IntegerField(primary_key=True)
    id_fase = models.ForeignKey(Fase, models.DO_NOTHING, db_column='id_fase')
    data = models.DateTimeField(blank=True, null=True)
    tensao = models.FloatField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fase_params'


class Geracao(models.Model):
    id_geracao = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey('Inversor', models.DO_NOTHING, db_column='id_inversor')
    data = models.DateTimeField(blank=True, null=True)
    geracao = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'geracao'


class GeracaoDiariaInversor(models.Model):
    id_geracao_diaria = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey('Inversor', models.DO_NOTHING, db_column='id_inversor')
    data = models.DateTimeField(blank=True, null=True)
    geracao = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'geracao_diaria_inversor'


class GeracaoDiariaPlanta(models.Model):
    id_geracao_diaria = models.AutoField(primary_key=True)
    id_planta_solar = models.ForeignKey('PlantaSolar', models.DO_NOTHING, db_column='id_planta_solar')
    geracao = models.FloatField(blank=True, null=True)
    data = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'geracao_diaria_planta'


class GeracaoPlanta(models.Model):
    id_geracao = models.AutoField(primary_key=True)
    id_planta_solar = models.ForeignKey('PlantaSolar', models.DO_NOTHING, db_column='id_planta_solar')
    geracao = models.FloatField(blank=True, null=True)
    data = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'geracao_planta'


class Inversor(models.Model):
    id_inversor = models.CharField(primary_key=True, max_length=100)
    id_planta_solar = models.ForeignKey('PlantaSolar', models.DO_NOTHING, db_column='id_planta_solar')
    modelo = models.CharField(max_length=45, blank=True, null=True)
    capacidade = models.FloatField(blank=True, null=True)
    potencia = models.FloatField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    geracao_dia = models.FloatField(blank=True, null=True)
    geracao_total = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inversor'


class InversorParams(models.Model):
    id_inversor_params = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey(Inversor, models.DO_NOTHING, db_column='id_inversor')
    data_horario = models.DateTimeField(blank=True, null=True)
    geracao_instantanea = models.FloatField(blank=True, null=True)
    freq_ca = models.FloatField(blank=True, null=True)
    temp_interna = models.FloatField(blank=True, null=True)
    geracao_diaria = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inversor_params'


class Mppt(models.Model):
    id_mppt = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey(Inversor, models.DO_NOTHING, db_column='id_inversor')
    nome = models.IntegerField()
    data = models.DateTimeField(blank=True, null=True)
    tensao = models.FloatField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mppt'


class MpptParams(models.Model):
    id_mppt_params = models.IntegerField(primary_key=True)
    id_mppt = models.ForeignKey(Mppt, models.DO_NOTHING, db_column='id_mppt')
    data = models.DateTimeField(blank=True, null=True)
    tensao = models.FloatField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mppt_params'


class PlantaSolar(models.Model):
    id_planta_solar = models.CharField(primary_key=True, max_length=100)
    id_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='id_cliente', blank=True, null=True)
    nome_planta = models.CharField(max_length=45, blank=True, null=True)
    localizacao = models.CharField(max_length=45, blank=True, null=True)
    monitoramento = models.IntegerField(blank=True, null=True)
    data_criacao = models.DateField(blank=True, null=True)
    classificacao = models.CharField(max_length=45, blank=True, null=True)
    usuario_plataforma_id_usuario_plataforma = models.ForeignKey('UsuarioPlataforma', models.DO_NOTHING, db_column='usuario_plataforma_id_usuario_plataforma', blank=True, null=True)
    num_modulos = models.IntegerField(blank=True, null=True)
    capacidade = models.FloatField(blank=True, null=True)
    foto = models.CharField(max_length=100, blank=True, null=True)
    geracao_diaria = models.FloatField(blank=True, null=True)
    geracao_total = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'planta_solar'


class StringMppt(models.Model):
    id_string_mppt = models.AutoField(primary_key=True)
    id_mppt = models.ForeignKey(Mppt, models.DO_NOTHING, db_column='id_mppt')
    data = models.DateTimeField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'string_mppt'
        unique_together = (('id_string_mppt', 'id_mppt'),)


class UsuarioPlataforma(models.Model):
    id_usuario_plataforma = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=45, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    senha = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuario_plataforma'
