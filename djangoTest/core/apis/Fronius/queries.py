import django

django.setup()

from core.models import *


# Insert plant basic info
def add_plant_basic_info(id_planta, nome_cliente, localizacao, capacidade, foto):
    planta = PlantaSolar()
    planta.id_planta_solar = id_planta
    planta.nome_planta = nome_cliente
    planta.localizacao = localizacao
    planta.capacidade = capacidade
    planta.foto = foto
    planta.save()


# Insert inverters
def add_inverter(id_inversor, id_planta, modelo, capacidade, status, numFases, numMppts):
    inversor = Inversor()
    inversor.id_inversor = id_inversor
    inversor.id_planta_solar = PlantaSolar.objects.get(id_planta_solar=id_planta)
    inversor.modelo = modelo
    inversor.capacidade = capacidade
    inversor.numMppts = numMppts
    inversor.numFases = numFases
    inversor.status = status
    inversor.save()


def update_plant_status(status, id_planta):
    planta = PlantaSolar.objects.get(id_planta_solar=id_planta)
    planta.monitoramento = status
    planta.save()


# Insert inverter mppt
def add_mppt(id_inversor, nome):
    mppt = Mppt()
    mppt.id_inversor = Inversor.objects.get(id_inversor=id_inversor)
    mppt.nome = nome
    mppt.save()


# Insert inverter mppts data
def add_mppt_params(data, id_inversor, params):
    for param in params:
        mppt = Mppt()
        mppt.id_inversor = Inversor.objects.get(id_inversor=id_inversor)
        mppt.data = data
        mppt.tensao = param[0]
        mppt.corrente = param[1]
        mppt.nome = param[2]
        mppt.save()


# Insert inverter params
def add_phases_params(device_id, date, phasesParams):

    for param in phasesParams:
        fase = Fase()
        fase.data = date
        fase.nome_fase = param[0]
        fase.id_inversor = Inversor.objects.get(id_inversor = device_id)
        fase.tensao = param[1]
        fase.corrente = param[2]
        fase.save()



def add_inverter_daily_generation(id_inversor, data, generation):
    geracao = GeracaoDiariaInversor()
    geracao.id_inversor = Inversor.objects.get(id_inversor=id_inversor)
    geracao.data = data
    geracao.geracao = generation
    geracao.save()


def add_inverter_instant_generation(id_inversor, log, geracao):
    instantGeneration = Geracao()
    instantGeneration.id_inversor = Inversor.objects.get(id_inversor=id_inversor)
    instantGeneration.data = log
    instantGeneration.geracao = geracao
    instantGeneration.save()


def add_plant_daily_generation(plant_id, data, generation):
    geracao = GeracaoDiariaPlanta()
    geracao.id_planta_solar = PlantaSolar.objects.get(id_planta_solar=plant_id)
    geracao.data = data
    geracao.geracao = generation
    geracao.save()


def add_plant_instant_generation(plant_id, data, generation):
    geracao = GeracaoPlanta()
    geracao.id_planta_solar = PlantaSolar.objects.get(id_planta_solar=plant_id)
    geracao.data = data
    geracao.geracao = generation
    geracao.save()
