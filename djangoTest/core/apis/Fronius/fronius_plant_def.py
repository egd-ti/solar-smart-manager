import json

import django
import item as item

django.setup()

from fronius_requests import FroniusApi
from datetime import datetime
from core.models import *
import requests
import queries
from collections import OrderedDict
import json


# Return current date rounded to 5 minutes
def get_current_date():
    minutes = int(datetime.now().minute / 5) * 5
    date = datetime.now().replace(minute=minutes).replace(second=0).strftime("%Y-%m-%dT%H:%M:%S").format()
    return date


def get_date():
    minutes = int(datetime.now().minute / 5) * 5
    date = datetime.now().replace(minute=minutes).replace(second=0).strftime("%Y-%m-%d %H:%M:%S").format()
    return date


# Return current day
def get_current_day():
    current_day = datetime.now().strftime("%Y-%m-%d").format()
    return current_day


class plant_def:
    frapi = FroniusApi()

    def __init__(self):
        self.session = requests.Session()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        return False

    # <-----------------PLANT BASE DATA------------------>
    # Add plant basic info
    def add_plant_base_info(self):
        plants = self.frapi.get_plants()
        for plant in plants['pvSystems']:
            id_planta = plant['pvSystemId']
            nome_cliente = plant['name']
            localizacao = plant['address']['city']
            capacidade = plant['peakPower'] / 1000
            foto = plant['pictureURL']
            queries.add_plant_basic_info(id_planta, nome_cliente, localizacao, capacidade, foto)

    def get_inverter_by_plant(self, plant_id):
        devices = self.frapi.get_devices(plant_id)
        for inverter in devices['devices']:
            id_inversor = inverter['deviceId']
            modelo_inversor = inverter['deviceName']
            capacidade = inverter['nominalAcPower'] / 1000
            status = inverter['isActive']
            numFases = int(inverter['numberPhases'])
            numMppts = int(inverter['numberMPPTrackers'])
            queries.add_inverter(id_inversor, plant_id, modelo_inversor, capacidade, status, numFases, numMppts)

    def add_inverters(self):
        plants = self.frapi.get_plant_ids()
        for plant in plants['pvSystemIds']:
            self.get_inverter_by_plant(plant)

    # <--------------------INVERTER BASE DATA-------------------->
    # Get the  phases params
    def get_phases_params(self, plant_id, device_id):
        date = get_current_date()
        phasesParams = []
        logs = json.dumps(
            self.frapi.get_phases_params(plant_id, device_id, date, date), indent=4)
        data = json.loads(logs)

        if data['data']:
            for channels in data['data'][0]['channels']:

                if 'VoltageA' in channels['channelName']:
                    phasesParams.insert(0, channels['channelName'][-1])
                    phasesParams.insert(1, channels['value'])
                if 'CurrentA' in channels['channelName']:
                    phasesParams.insert(2, channels['value'])
                if 'VoltageB' in channels['channelName']:
                    phasesParams.insert(3, channels['channelName'][-1])
                    phasesParams.insert(4, channels['value'])
                if 'CurrentB' in channels['channelName']:
                    phasesParams.insert(5, channels['value'])
                if 'VoltageC' in channels['channelName']:
                    phasesParams.insert(6, channels['channelName'][-1])
                    phasesParams.insert(7, channels['value'])
                if 'CurrentC' in channels['channelName']:
                    phasesParams.insert(8, channels['value'])

        params = [phasesParams[i:i + 3] for i in range(0, len(phasesParams), 3)]
        queries.add_phases_params(device_id, date, params)

    # Get the inverter params
    def get_inverter_mppt_params(self, paramsInv, plant_id, device_id):
        queryParams = []
        logs = json.dumps(
            self.frapi.get_inverter_params(plant_id, device_id, get_current_date(), get_current_date()), indent=4)
        data = json.loads(logs)
        if data['data']:
            log = data['data'][0]['logDateTime']
            for channels in data['data'][0]['channels']:
                if any('DC' in x for x in paramsInv):
                    if 'VoltageDC' in channels['channelName']:
                        queryParams.append(channels['value'])
                    if 'CurrentDC' in channels['channelName']:
                        queryParams.append(channels['value'])
                        queryParams.append(int(channels['channelName'][-1]))

            params = [queryParams[i:i + 3] for i in range(0, len(queryParams), 3)]
            queries.add_mppt_params(log, device_id, params)

    # Get plant network status
    def get_plant_status(self):
        plants = self.frapi.get_plant_ids()
        for plant in plants['pvSystemIds']:
            status = self.frapi.get_plant_status(plant)
            monitoramento = int(status['status']['isOnline'])
            queries.update_plant_status(monitoramento, plant)

    # Get mppts infoo
    def get_mppt(self, plant_id, device_id):
        params = ['VoltageDC', 'CurrentDC']
        self.get_inverter_mppt_params(params, plant_id, device_id)

    # <---------------------INVERTER GENERATION DATA--------------------->
    # Get daily generation by inverter
    def get_daily_inverter_generation(self, plant_id, device_id):
        invDailyGeneration = json.dumps(
            self.frapi.get_daily_inverter_generation(plant_id, device_id, get_current_day(), get_current_day()))
        data = json.loads(invDailyGeneration)
        if data['data']:
            for channels in data['data'][0]['channels']:
                generation = round(channels['value'] / 1000, 2)
                queries.add_inverter_daily_generation(device_id, get_current_day(), generation)

    # Get inverter generation per minute
    def get_inverter_instant_generation(self, plant_id, device_id):
        logs = json.dumps(
            self.frapi.get_instant_inverter_generation(plant_id, device_id, get_current_date(),
                                                       get_current_date()), indent=4)
        data = json.loads(logs)
        if data['data']:
            log = data['data'][0]['logDateTime']
            for channels in data['data'][0]['channels']:
                if 'EnergyExported' in channels['channelName']:
                    generation = round(channels['value'] / 1000, 2)
                    queries.add_inverter_instant_generation(device_id, log, generation)

    def get_all_inverter_generation(self):
        for inversor in Inversor.objects.all():
            self.get_inverter_instant_generation(inversor.id_planta_solar.id_planta_solar, inversor.id_inversor)
            self.get_daily_inverter_generation(inversor.id_planta_solar.id_planta_solar, inversor.id_inversor)

    # <---------------------PLANT GENERATION DATA--------------------->
    # Get daily plant generation
    def get_daily_plant_generation(self, plant_id, date_start, date_end):
        dailyGeneration = self.frapi.get_daily_plant_generation(plant_id, date_start, date_end)
        for generation in dailyGeneration['data']:
            for channels in generation['channels']:
                if any(x in channels['channelName'] for x in ['EnergyOutput']):
                    generation = round(channels['value'] / 1000, 2)
                    queries.add_plant_daily_generation(plant_id, get_date(), generation)

    # Get instant plant generation
    def get_instant_plant_generation(self, plant_id):
        plantDailyGeneration = self.frapi.get_plant_status(plant_id)
        if plantDailyGeneration['data'] is not None:
            for generation in plantDailyGeneration['data']['channels']:
                if 'PowerOutput' in generation['channelName']:
                    if generation:
                        generationD = round(generation['value'] / 1000, 2)
                        queries.add_plant_instant_generation(plant_id, get_date(), generationD)

    def get_all_plants_generation(self):
        for planta in PlantaSolar.objects.all():
            self.get_daily_plant_generation(planta.id_planta_solar, get_current_day(), get_current_day())
            self.get_instant_plant_generation(planta.id_planta_solar)

    def get_all_mppts(self):
        for inversor in Inversor.objects.all():
            self.get_mppt(inversor.id_planta_solar.id_planta_solar, inversor.id_inversor)

    def get_all_phases(self):
        for inversor in Inversor.objects.all():
            self.get_phases_params(inversor.id_planta_solar.id_planta_solar, inversor.id_inversor)


plf = plant_def()
# plf.get_inverter_instant_generation("cfb0f29d-e92b-476e-bef2-a81d01431e3a", "160a71b0-e061-45d3-ab33-bfe43df586e6")
# plf.get_daily_plant_generation("160a71b0-e061-45d3-ab33-bfe43df586e6", get_current_day(), get_current_day())
plf.get_phases_params("160a71b0-e061-45d3-ab33-bfe43df586e6", "99cafd4a-8468-40b4-8d93-2520007cf0ef")
