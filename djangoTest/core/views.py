from django.http import Http404
from django.shortcuts import render
from core.models import PlantaSolar
from django.views.generic import TemplateView
from django.db import connection

# Create your views here.


def detalhamento(request, id_planta_solar):
    try:
        planta = PlantaSolar.objects.get(pk=id_planta_solar)

    except planta.DoesNotExist:
        raise Http404('A planta não existe!')
    return render(request, 'detalhamento.html', context={'planta': planta})


def countOnlinePlants():
    online = 0
    for planta in PlantaSolar.objects.all():
        if planta.monitoramento == 1:
            online += 1
    return online


class IndexView(TemplateView):
    template_name = "visao_geral.html"

    def get_context_data(self, **kwargs):

        context = super(IndexView, self).get_context_data(**kwargs)
        context['plantas'] = PlantaSolar.objects.all()
        context['plantasOnline'] = countOnlinePlants()
        context['totalPlantas'] = PlantaSolar.objects.count()

        return context
