# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True  # specify this model as an Abstract Model
        app_label = 'ssm'


class Alerta(models.Model):
    id_alerta = models.IntegerField(primary_key=True)
    id_planta_solar = models.ForeignKey('PlantaSolar', models.DO_NOTHING, db_column='id_planta_solar')
    data_horario = models.DateTimeField(blank=True, null=True)
    tipo = models.CharField(max_length=45, blank=True, null=True)
    mensagem = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'alerta'


class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    nome_cliente = models.CharField(max_length=45, blank=True, null=True)
    endereco = models.CharField(max_length=45, blank=True, null=True)
    contato = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'cliente'


class PlantaSolar(models.Model):
    id_planta_solar = models.CharField(primary_key=True, max_length=100)
    id_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='id_cliente', blank=True, null=True)
    nome_planta = models.CharField(max_length=45, blank=True, null=True)
    localizacao = models.CharField(max_length=45, blank=True, null=True)
    monitoramento = models.IntegerField(blank=True, null=True)
    data_criacao = models.DateField(blank=True, null=True)
    classificacao = models.CharField(max_length=45, blank=True, null=True)
    num_modulos = models.IntegerField(blank=True, null=True)
    capacidade = models.FloatField(blank=True, null=True)
    foto = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'planta_solar'


class Inversor(models.Model):
    id_inversor = models.CharField(primary_key=True, max_length=100)
    planta = models.ForeignKey('PlantaSolar', models.DO_NOTHING, db_column='id_planta_solar', related_name='inversores')
    modelo = models.CharField(max_length=45, blank=True, null=True)
    capacidade = models.FloatField(blank=True, null=True)
    potencia = models.FloatField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    geracao_dia = models.FloatField(blank=True, null=True)
    geracao_total = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'inversor'


class Fase(models.Model):
    id_fase = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey('Inversor', models.DO_NOTHING, db_column='id_inversor')
    tensao = models.FloatField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)
    data = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'fase'


class InversorParams(models.Model):
    id_inversor_params = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey(Inversor, models.DO_NOTHING, db_column='id_inversor', related_name='invParams')
    data_horario = models.DateTimeField(blank=True, null=True)
    geracao = models.FloatField(blank=True, null=True)
    freq_ca = models.FloatField(blank=True, null=True)
    temp_interna = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'inversor_params'


class Mppt(models.Model):
    id_mppt = models.AutoField(primary_key=True)
    id_inversor = models.ForeignKey(Inversor, models.DO_NOTHING, db_column='id_inversor')
    nome = models.IntegerField()
    data = models.DateTimeField(blank=True, null=True)
    tensao = models.FloatField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'mppt'


class StringMppt(models.Model):
    id_string_mppt = models.AutoField(primary_key=True)
    id_mppt = models.ForeignKey(Mppt, models.DO_NOTHING, db_column='id_mppt')
    data = models.DateTimeField(blank=True, null=True)
    corrente = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'string_mppt'
        unique_together = (('id_string_mppt', 'id_mppt'),)


class UsuarioPlataforma(models.Model):
    id_usuario_plataforma = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=45, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    senha = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'usuario_plataforma'
