import requests
import json



session = requests.Session()

url = 'https://monitoring.csisolar.com/maintain/plant'
headers = {'Authorization':'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxOTQ5MV9nZXN0YW9AZXNjb2dkLmNvbS5icl8yIiwic2NvcGUiOlsiYWxsIl0sImRldGFpbCI6eyJvcmdhbml6YXRpb25JZCI6MTk0OTEsInRvcEdyb3VwSWQiOjE1MDE3LCJncm91cElkIjoxNTAxNywicm9sZUlkIjoxLCJ1c2VySWQiOjg1NzEyLCJ2ZXJzaW9uIjoxLCJpZGVudGlmaWVyIjoiZ2VzdGFvQGVzY29nZC5jb20uYnIiLCJpZGVudGl0eVR5cGUiOjIsImFwcElkIjpudWxsfSwiZXhwIjoxNjE4Njc5ODEzLCJhdXRob3JpdGllcyI6WyJhbGwiXSwianRpIjoiYzg0ZGEwNzAtZDdiOS00YTAyLTgzYTYtODkyYTcwYmMxOGNhIiwiY2xpZW50X2lkIjoidGVzdCJ9.OEI1ocuYzGz3PL2SRieoQdkt8WbSwgB5VjFe7d57GCHFhh50kFYUIfB7CScVwVK-TPv-hKVAEHVAv-Ajfde9nG0ITUXns7Z5ySXBtErf77JQHfI5XDEaNLKcEM6YlFX6QBmEeYiJrUEzVTI57btMB4NNNoAG0iWp-QyuPHO7BVtHOr7lPIB3Bdw-xHhgAzJludkJWnfCCrP5D4XPxULxEwPa3PSb6keZc5ttoqienlqFS5UoujATHANgjFXfJa35DjRq6j9PsZd8y2jsIyNu2TkBFIANOnf3ZDG1Q14XV1EiyKQps0l28TjCVsF9RI_gQRNAamzKnj_Xe1aumHQBQw'}
payload = {'Request Payload':'{"region":{"nationId":null,"level1":null,"level2":null,"level3":null,"level4":null,"level5":null},"externalRelationship":{},"powerTypeList":["PV"]}'}

response = session.post(url, headers=headers)

print(response)
#data = response.json()

#print(data)